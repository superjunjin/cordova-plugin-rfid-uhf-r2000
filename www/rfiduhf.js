var exec = require('cordova/exec');

exports.open = function (success, error) {
	exec(success, error, "RfidUhf", "open", []);
};

exports.close = function () {
	exec(null, null, "RfidUhf", "close", []);
};